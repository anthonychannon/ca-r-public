DECLARE @StartofBookings date
              ,@EndofBookings date
SELECT @StartofBookings = '20180901'
              ,@EndofBookings = '20180930'

-- Select bookings from relevant time frame

IF OBJECT_ID('tempdb.dbo.#Bookings') IS NOT NULL
DROP TABLE #Bookings

SELECT CustomerID
              ,EmailHash
			  ,HaulType
			  ,ShopChannel
			  ,CASE WHEN HardDiff = 1 THEN 'HardDiff' WHEN SoftDiff = 1 THEN 'SoftDiff' ELSE 'Commodity' END AS DiffType
			  ,CASE WHEN ProductName = 'Flight Only' THEN 'Flight Only' WHEN ProductName = 'Cruise' THEN 'Cruise' WHEN BookingSeason LIKE 'W%' THEN 'Winter' ELSE 'Beach' END AS ProductType
			  ,CASE WHEN CruiseShipName!='' THEN 'Marella Cruises' WHEN ProductBrand='Thomson' THEN 'TUI' WHEN ProductBrand='First Choice' THEN 'First Choice' ELSE 'Other' END AS ProdBrand
              ,BookingSummaryID
              ,BookedDate
              ,TotalHolidayCost
              ,NumberofPAX
INTO #Bookings
FROM vwfactBookingSummary vfbs
WHERE BookingStatusDescription = 'Confirmed'
AND TUIMainstream = 1
AND BookedDate >= @StartofBookings AND BookedDate <= @Endofbookings

-- Aggregate those bookings at a customer level for comparison

IF OBJECT_ID('tempdb.dbo.#AggBookings') IS NOT NULL
DROP TABLE #AggBookings

SELECT CustomerID
              ,EmailHash
			  ,HaulType
			  ,ShopChannel
			  ,DiffType
			  ,ProductType
			  ,ProdBrand
			  ,CASE WHEN b.CustomerID = -1 THEN 'MinusOne' ELSE 'HasID' END AS HasID
			  ,BookedDate
              ,COUNT(BookingSummaryID) AS NumberOfBookings
              ,SUM(b.TotalHolidaycost) AS TotalRevenue
              ,SUM(b.NumberOfPAX) AS TotalPAX
INTO #AggBookings
FROM #Bookings b
GROUP BY CustomerID, EmailHash, HaulType, ShopChannel, DiffType, ProductType, ProdBrand, CASE WHEN b.CustomerID = -1 THEN 'MinusOne' ELSE 'HasID' END, BookedDate

-- Compare Fallow and Non-Fallow                                                            */

IF OBJECT_ID('tempdb.dbo.#Totals') IS NOT NULL
DROP TABLE #Totals

SELECT f.Fallow_flag, a.HaulType, ShopChannel, DiffType, ProductType, ProdBrand, HasID, BookedDate
              ,SUM(a.NumberOfBookings) AS Bookings
              ,SUM(a.TotalRevenue) AS Revenue
              ,SUM(a.TotalPAX) AS PAX 
INTO #Totals
FROM #AggBookings a
INNER JOIN TUI_Temporary.dbo.JWFallow2017 f
ON a.CustomerID = f.CustomerID
AND a.EmailHash = f.EmailHash
GROUP BY f.Fallow_Flag, a.HaulType, ShopChannel, DiffType, ProductType, ProdBrand, HasID, BookedDate


ALTER TABLE #Totals
ADD Members int

UPDATE #Totals
SET Members = a.Members
FROM #Totals t
INNER JOIN (
                     SELECT Fallow_Flag, COUNT(DISTINCT RandomId) AS Members
                     FROM TUI_Temporary.dbo.JWFallow2017
                     GROUP BY Fallow_Flag
                     ) as a
ON t.Fallow_Flag = a.Fallow_Flag

--Add Rates to Totals table

ALTER TABLE #Totals
ADD BookingRate float, RevenueRate float, PAXRate float, AverageBookingRevenue float

UPDATE #Totals
SET BookingRate = (1.0*Bookings)/(1.0*Members),
       RevenueRate = (1.0*Revenue)/(1.0*Members),
       PAXRate = (1.0*PAX)/(1.0*Members),
	   AverageBookingRevenue = (1.0*Revenue)/(1.0*Bookings)

DECLARE @Count bigint =
(SELECT COUNT(*)
FROM(
       SELECT DISTINCT e.EmailHash, e.CustomerID
       FROM DWPresentation.dbo.vwfactbrandemail e
       JOIN DWPresentation.dbo.vwfactMembers m
              ON e.BrandName = m.BrandName
                     AND e.EmailHash = m.EmailHash
                     AND m.membertype ='normal'
                     AND m.islyris11 = 1
                     AND e.Optin = 1
                     AND e.EmailAddress != 'Unknown'
       EXCEPT

          SELECT DISTINCT EmailHash, CustomerID
          FROM TUI_Temporary.dbo.JWFallow2017
          WHERE Fallow_Flag = 1
          ) AS total
)

DECLARE @OrigFallowCandidates float = (SELECT COUNT(DISTINCT RandomId) FROM TUI_Temporary.dbo.JWFallow2017) 
DECLARE @OrigFallowCombinations float = (SELECT COUNT(*) FROM TUI_Temporary.dbo.JWFallow2017)
DECLARE @Factor float = ( @OrigFallowCandidates / @OrigFallowCombinations ) * ( @Count )

SELECT HaulType,
	@Factor * SUM(RevenueRate * CASE WHEN Fallow_Flag = 1 THEN -1.0 ELSE 1.0 END) AS IncrementalRevenue
	--,1.2 * @Factor * SUM(RevenueRate * CASE WHEN Fallow_Flag = 1 THEN -1.0 ELSE 1.0 END) AS IncrementalRevenueEuros
FROM #Totals
GROUP BY HaulType

SELECT ShopChannel,
	@Factor * SUM(RevenueRate * CASE WHEN Fallow_Flag = 1 THEN -1.0 ELSE 1.0 END) AS IncrementalRevenue
	--,1.2 * @Factor * SUM(RevenueRate * CASE WHEN Fallow_Flag = 1 THEN -1.0 ELSE 1.0 END) AS IncrementalRevenueEuros
FROM #Totals
GROUP BY ShopChannel

SELECT DiffType,
	@Factor * SUM(RevenueRate * CASE WHEN Fallow_Flag = 1 THEN -1.0 ELSE 1.0 END) AS IncrementalRevenue
	--,1.2 * @Factor * SUM(RevenueRate * CASE WHEN Fallow_Flag = 1 THEN -1.0 ELSE 1.0 END) AS IncrementalRevenueEuros
FROM #Totals
GROUP BY DiffType

SELECT ProductType,
	@Factor * SUM(RevenueRate * CASE WHEN Fallow_Flag = 1 THEN -1.0 ELSE 1.0 END) AS IncrementalRevenue
	--,1.2 * @Factor * SUM(RevenueRate * CASE WHEN Fallow_Flag = 1 THEN -1.0 ELSE 1.0 END) AS IncrementalRevenueEuros
FROM #Totals
GROUP BY ProductType

SELECT ProdBrand,
	@Factor * SUM(RevenueRate * CASE WHEN Fallow_Flag = 1 THEN -1.0 ELSE 1.0 END) AS IncrementalRevenue
	--,1.2 * @Factor * SUM(RevenueRate * CASE WHEN Fallow_Flag = 1 THEN -1.0 ELSE 1.0 END) AS IncrementalRevenueEuros
FROM #Totals
GROUP BY ProdBrand

SELECT HasID,
	@Factor * SUM(RevenueRate * CASE WHEN Fallow_Flag = 1 THEN -1.0 ELSE 1.0 END) AS IncrementalRevenue
	--,1.2 * @Factor * SUM(RevenueRate * CASE WHEN Fallow_Flag = 1 THEN -1.0 ELSE 1.0 END) AS IncrementalRevenueEuros
FROM #Totals
GROUP BY HasID

SELECT YEAR(BookedDate), MONTH(BookedDate),
	@Factor * SUM(RevenueRate * CASE WHEN Fallow_Flag = 1 THEN -1.0 ELSE 1.0 END) AS IncrementalRevenue,
	1.2 * @Factor * SUM(RevenueRate * CASE WHEN Fallow_Flag = 1 THEN -1.0 ELSE 1.0 END) AS IncrementalRevenueEuros
FROM #Totals
GROUP BY YEAR(BookedDate), MONTH(BookedDate)
ORDER BY YEAR(BookedDate), MONTH(BookedDate)


