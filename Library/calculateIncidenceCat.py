def calculateIncidenceCat(df, col_name, target, restriction = 'percentage',
                           nbCat = 10, minPerc = 0.005, makePlots = True, 
                           saveIncidence = True):
    """Calculates incidences

    Parameters
    ----------
    data_frame : pandas DataFrame         
       
    column_name : str
        name of a column with categorical values

    target : str
        name of a target column

    restriction : str
        either 'percentage' to select all categories containing more than
        some threshold or 'topX' for top X (int) categories
    
    nbCat : int
        number of categories for 'topX' restriction
    
    minPerc : float
        threshold to select categories
    
    makePlots : bool
        producing plots for exploration (default = True)
    
    saveIncidence : bool
        produces dictionary of incidence replacements (default = True)
    
    Result
    ----------
    if saveIncidence == True returns incidence dictionary

    Warning
    -------
    no validation yet

    """
    # name of new feature with the incidences
    new_col_name = col_name + '_inc_' + target
    
    # % of the population for each value within col_name
    prob = df[col_name].value_counts(normalize = True, dropna = False)
    
    if restriction == 'topX':
        keepCat = prob[0:nbCat].index.values.tolist()
    elif restriction == 'percentage':
        keepCat = prob[prob > minPerc].index.values.tolist()
            
    kwargs = {new_col_name : df[col_name]}
    df = df.assign(**kwargs)    
    df[new_col_name].fillna('Missing', inplace = True)
    
    # handling missing and excluded values
    keepCat.append('Missing')      
    df.loc[~df[new_col_name].isin(keepCat), new_col_name] = "Other"
    
    # replacement
    df[new_col_name].replace()
         
    # prepare data for plots and for saving into dictionaries
    infoLinePlot = df.groupby(new_col_name)[target].agg('mean').reset_index()
    infoBarPlot = (df[new_col_name]
                    .value_counts(normalize = True)
                    .reset_index()
                    .rename({'index':new_col_name,
                             new_col_name : 'frequency'}, axis = 1))

    
    if makePlots:
        df = pd.merge(infoLinePlot, infoBarPlot, how = 'left', 
                      on = new_col_name)
        df = (df.sort_values('frequency', ascending = False)
                .reset_index(drop = True))
        # define figure size
        plt.figure(figsize=(8,8))
        
        # subplot 1
        plt.subplot(211)
        plt.bar(df[new_col_name], df['frequency'], 
                color=(112/255, 203/255, 244/255, 1),
                label = '% of population')
        for a, b in zip(df[new_col_name], df['frequency']): 
            plt.text(a, b, str('{:,.2%}'.format(b)),size=7, ha = 'center')
        plt.xticks(rotation=45)
        plt.title('incidence for '+col_name+' wrt '+target)
        plt.legend()
        
        # subplot 2
        plt.subplot(212)
        plt.plot(df[new_col_name], 
                 df[target],
                 marker='d',
                    color=(112/255, 203/255, 244/255, 1),
                    label = '% of conversion')
        
        for a,b in zip(df[new_col_name], df[target]): 
            plt.text(a, b, str('{:,.2%}'.format(b)),size=7, ha = 'center')
        plt.xticks(rotation=45)
        plt.tight_layout()
        plt.legend()
        plt.ylim(ymin=0, ymax = df[target].max() + 0.1)
        # save plot
        if not 'Figures' in os.listdir():
            os.mkdir('Figures')
            
        plt.savefig(os.getcwd()+
                    '/Figures/incidencefor'+col_name+'wrt'+target+'.png')
    
    # save incidence calulations to pickle
    if saveIncidence:
        # save all info in dictionary
        incidence = {'new_col_name': new_col_name,
                     'col_name' : col_name}                       
        incidence.update({value:incidence for value, incidence in zip(
                infoLinePlot[new_col_name], infoLinePlot[target])})    
        return incidence


