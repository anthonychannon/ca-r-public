def distance_calc(dep_lat, dep_lon, arr_lat, arr_lon):
    R = 6373.0
    dep_lat = radians(dep_lat)
    dep_lon = radians(dep_lon)
    arr_lat = radians(arr_lat)
    arr_lon = radians(arr_lon)

    dlon = dep_lon - arr_lon
    dlat = dep_lat - arr_lat

    a = sin(dlat / 2)**2 + cos(dep_lat) * cos(arr_lat) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))
    return(R * c)