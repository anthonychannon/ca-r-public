def calculateCountsCat(data_frame, column_name, threshold):
    """Counts percentages of populations for a given column with a certain threshold

    Parameters
    ----------
    df : pandas DataFrame         
       
    column_name : str
        column name with categorical values

    threshold : float

    Result
    ----------
    returns pandas Series

    Warning
    -------
    no validation yet

    """

    prob = data_frame[column_name].value_counts(normalize = True)
    mask = prob > threshold
    tail_prob = prob.loc[~mask].sum()
    prob = prob.loc[mask]
    prob['other'] = tail_prob
    return prob
