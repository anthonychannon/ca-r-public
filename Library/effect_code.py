def effect_code(df, column, values):
    col = df[column]
    eff = {}
    for i in range(len(values) - 1):
        eff[column + '_e{}'.format(i)] = col.apply(lambda x: 1.0 if x == values[i + 1] else (-1.0 if x == values[0] else 0.0))
    
    return df.assign(**eff)