def circular_transformation(df, col_name, period_length = 24):
    """transforms one periodic pd.series into two pd.series 
    
    input:
        
    df - pandas dataframe
    col_name - column with periodical values (hours, days, months, etc.)
    period_length - max units in a period (24 hours in a day, 60 seconds in 
    one hour, 7 days in a week)
    
    no validation yet
    
    """
    s = df[col_name]
    s_x = s.apply(lambda x: np.sin(x / period_length * 2 * np.pi))
    s_y = s.apply(lambda x: np.cos(x / period_length * 2 * np.pi))
    kwargs = {col_name + '_cx': s_x,
              col_name + '_cy': s_y}
    return df.assign(**kwargs)
                                  